<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription =array(
    "NAME" => GetMessage("COMP_NAME"),
    "DESCRIPTION" => GetMessage("COMP_DESCR"),
    "ICON" => "/images/Pg.gif",
    "SORT" => 20,
    "CACHE_PATH" => "Y",
    "PATH" => array(
        "ID" => "new_component",
        "CHILD" => array(
            "ID" => "Новый Компонент",
            "NAME" => GetMessage("T_IBLOCK_DESC_NEWS"),
            "SORT" => 10,
        ),
    ),

);

