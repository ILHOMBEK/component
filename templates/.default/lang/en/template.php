<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["NAME"] = "Name";
$MESS["LINK"] = "Link";
$MESS["FILE_SIZE"] = "File Size";
$MESS["FILE_TYPE"] = "File Type";
$MESS["DATE_OF_UPDATE"] = "Date of update";
$MESS["DIVISIONS"] = "Divisions";
?>