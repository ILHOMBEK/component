<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();



//debug($arResult);
//debug($arParams);

?>

<table border="1" width="100%" cellpadding="0" cellspacing="0">
    <thead>
        <th colspan="3"><?=GetMessage("NAME")?></th>
        <?if ($arParams["DIVISION"] == "Y"){?>
            <th colspan="3"><?=GetMessage("DIVISIONS")?></th>
        <?}?>
        <th colspan="3"><?=GetMessage("LINK")?></th>
        <?if ($arParams["FILE_SIZE"] == "Y"){?>
            <th colspan="3"><?=GetMessage("FILE_SIZE")?></th>
        <?}?>
        <?if ($arParams["FILE_TYPE"] == "Y"){?>
            <th colspan="3"><?=GetMessage("FILE_TYPE")?></th>
        <?}?>
        <?if ($arParams["TIMESTAMP_X"] == "Y"){?>
            <th colspan="3"><?=GetMessage("DATE_OF_UPDATE")?></th>
        <?}?>
    </thead>
    <tbody align="center" >
        <? foreach($arResult["ITEMS"] as $arItem){?>
            <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>

            <tr id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <td colspan="3"><?=$arItem["NAME"]?></td>
                <?if ($arParams["DIVISION"] == "Y"){?>
                    <td colspan="3"><?=$arItem["PROPERTY_DIVISIONS_VALUE"]?></td>
                <?}?>
                <td colspan="3"><a href="<?=$arItem["FILE_INFO"]["SRC"]?>" download>скачать</a></td>
                <?if ($arParams["FILE_SIZE"] == "Y"){?>
                    <td colspan="3"><?=$arItem["FILE_INFO"]["FILE_SIZE"]?></td>
                <?}?>
                <?if ($arParams["FILE_TYPE"] == "Y"){?>
                    <td colspan="3"><?=$arItem["FILE_INFO"]["EXTENSION"]?></td>
                <?}?>
                <?if ($arParams["TIMESTAMP_X"] == "Y"){?>
                    <td colspan="3"><?=date("d.m.Y", strtotime($arItem["FILE_INFO"]["TIMESTAMP_X"]))?></td>
                <?}?>
            </tr>
        <?}?>
    </tbody>
</table>

