<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


//debug($arParams);
CModule::IncludeModule("iblock");



$arFilter = array(
    "IBLOCK_TYPE"=>$arParams["IBLOCK_TYPE"],
    "IBLOCK_ID"=>$arParams["IBLOCK_ID"],
    "ACTIVE"=>"Y",
);

$arOrder = array($arParams["SORT_BY1"] => $arParams["SORT_ORDER1"]);

$res = CIBlockElement::GetList(
    $arOrder,
    $arFilter,
    false,
    false,
    Array("ID", "NAME","PROPERTY_FILE", "PROPERTY_DIVISIONS")
);


while($result = $res->Fetch()){
//debug($result);
    $arFile = CFile::GetFileArray($result["PROPERTY_FILE_VALUE"]);
    $arFile['FILE_SIZE'] = CFile::FormatSize($arFile['FILE_SIZE']);
    $arFile["EXTENSION"] = GetFileExtension($arFile["SRC"]);
    $result['FILE_INFO'] = $arFile;


    $arResult["ITEMS"][] = $result;

}

//debug($arResult);
//debug($result);

$this->IncludeComponentTemplate();
?>