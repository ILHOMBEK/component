<?

$MESS["T_IBLOCK_DESC_LIST_ID"] = "Information block code";//
$MESS["T_IBLOCK_DESC_LIST_TYPE"] = "Type of information block (used for verification only)";//
$MESS["T_IBLOCK_PROPERTY"] = "Properties";//
$MESS["DIVISION"] = "Display department";
$MESS["FILE_SIZE"] = "Output file size";
$MESS["FILE_TYPE"] = "Output file type";
$MESS["TIMESTAMP_X"] = "Display update date";
?>